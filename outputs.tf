output "task_definition_family" {
  description = "The family of the Task Definition."
  value       = join("", aws_ecs_task_definition.this.*.family)
}

output "task_definition_revision" {
  description = "The revision of the task in a particular family."
  value       = join("", aws_ecs_task_definition.this.*.revision)
}

output "task_definition_arn" {
  description = "Full ARN of the Task Definition (including both family and revision)."
  value       = join("", aws_ecs_task_definition.this.*.arn)
}

output "task_role_arn" {
  description = "ARN of the IAM Task role."
  value       = var.ecs_task_role_arn != "" ? var.ecs_task_role_arn : join("", aws_iam_role.task_role.*.arn)
}
