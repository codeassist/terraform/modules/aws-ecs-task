# aws-ecs-task

Terraform module to create an ECS Task from the Container definition.

---

## Usage
```hcl
  provider "aws" {
    region = var.region
  }

  module "label" {
    source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=tags/0.15.0"
    namespace  = var.namespace
    name       = var.name
    stage      = var.stage
    delimiter  = var.delimiter
    attributes = var.attributes
    tags       = var.tags
  }

  module "vpc" {
    source     = "git::https://github.com/cloudposse/terraform-aws-vpc.git?ref=tags/0.8.1"
    namespace  = var.namespace
    stage      = var.stage
    name       = var.name
    delimiter  = var.delimiter
    attributes = var.attributes
    cidr_block = var.vpc_cidr_block
    tags       = var.tags
  }

  module "subnets" {
    source               = "git::https://github.com/cloudposse/terraform-aws-dynamic-subnets.git?ref=tags/0.16.1"
    availability_zones   = var.availability_zones
    namespace            = var.namespace
    stage                = var.stage
    name                 = var.name
    attributes           = var.attributes
    delimiter            = var.delimiter
    vpc_id               = module.vpc.vpc_id
    igw_id               = module.vpc.igw_id
    cidr_block           = module.vpc.vpc_cidr_block
    nat_gateway_enabled  = true
    nat_instance_enabled = false
    tags                 = var.tags
  }

  resource "aws_ecs_cluster" "default" {
    name = module.label.id
    tags = module.label.tags
  }

  module "container_definition" {
    source                       = "git::https://github.com/cloudposse/terraform-aws-ecs-container-definition.git?ref=tags/0.21.0"
    container_name               = var.container_name
    container_image              = var.container_image
    container_memory             = var.container_memory
    container_memory_reservation = var.container_memory_reservation
    container_cpu                = var.container_cpu
    essential                    = var.container_essential
    readonly_root_filesystem     = var.container_readonly_root_filesystem
    environment                  = var.container_environment
    port_mappings                = var.container_port_mappings
    log_configuration            = var.container_log_configuration
  }

  module "ecs_alb_service_task" {
    source                             = "git::https://github.com/cloudposse/terraform-aws-ecs-alb-service-task.git?ref=master"
    namespace                          = var.namespace
    stage                              = var.stage
    name                               = var.name
    attributes                         = var.attributes
    delimiter                          = var.delimiter
    alb_security_group                 = module.vpc.vpc_default_security_group_id
    container_definition_json          = module.container_definition.json
    ecs_cluster_arn                    = aws_ecs_cluster.default.arn
    launch_type                        = var.ecs_launch_type
    vpc_id                             = module.vpc.vpc_id
    security_group_ids                 = [module.vpc.vpc_default_security_group_id]
    subnet_ids                         = module.subnets.public_subnet_ids
    tags                               = var.tags
    ignore_changes_task_definition     = var.ignore_changes_task_definition
    network_mode                       = var.network_mode
    assign_public_ip                   = var.assign_public_ip
    propagate_tags                     = var.propagate_tags
    health_check_grace_period_seconds  = var.health_check_grace_period_seconds
    deployment_minimum_healthy_percent = var.deployment_minimum_healthy_percent
    deployment_maximum_percent         = var.deployment_maximum_percent
    deployment_controller_type         = var.deployment_controller_type
    desired_count                      = var.desired_count
    task_memory                        = var.task_memory
    task_cpu                           = var.task_cpu
  }
```

The `container_image` in the `container_definition` module is the Docker image used to start a container.

This string is passed directly to the Docker daemon. Images in the Docker Hub registry are available by default.
Other repositories are specified with either `repository-url/image:tag` or `repository-url/image@digest`.
Up to 255 letters (uppercase and lowercase), numbers, hyphens, underscores, colons, periods, forward slashes, and number signs are allowed.
This parameter maps to Image in the Create a container section of the Docker Remote API and the IMAGE parameter of `docker run`.

When a new task starts, the Amazon ECS container agent pulls the latest version of the specified image and tag for the container to use.
However, subsequent updates to a repository image are not propagated to already running tasks.

Images in Amazon ECR repositories can be specified by either using the full `registry/repository:tag` or `registry/repository@digest`.
For example, `012345678910.dkr.ecr.<region-name>.amazonaws.com/<repository-name>:latest` or `012345678910.dkr.ecr.<region-name>.amazonaws.com/<repository-name>@sha256:94afd1f2e64d908bc90dbca0035a5b567EXAMPLE`.

Images in official repositories on Docker Hub use a single name (for example, `ubuntu` or `mongo`).

Images in other repositories on Docker Hub are qualified with an organization name (for example, `amazon/amazon-ecs-agent`).

Images in other online repositories are qualified further by a domain name (for example, `quay.io/assemblyline/ubuntu`).

For more info, see [Container Definition](https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_ContainerDefinition.html).


## Inputs

`see: variables.tf`

## Outputs
`see: outputs.tf`

## License 

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) 

See [LICENSE](LICENSE) for full details.

    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

      https://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.


## Trademarks

All other trademarks referenced herein are the property of their respective owners.
