# --------------------------
# Common/General parameters
# --------------------------
# Whether to create the resources ("false" prevents the module from creating any resources).
ecs_task_module_enabled = false
# Emulation of `depends_on` behavior for the module.
ecs_task_module_depends_on = []
# (Required) A unique name for your task definition.
ecs_task_family = "test2"
# Key-value mapping of resource tags.
#ecs_task_tags = {}


# ---------------
# IAM parameters
# ---------------
# The JSON formatted string representing user-defined IAM policy to allow ECS Task call AWS Services on our behalf.
ecs_task_policy = ""
# In order to avoid error `count can't be computed` specify `true` here when a custom role must be associated.
ecs_task_custom_role_specified = false
# The ARN of custom IAM role to associate with the Task instead of creating one.
ecs_task_role_arn = ""


# --------------------
# ECS Task parameters
# --------------------
# (Required) A list of valid container definitions provided as a single valid JSON document.
# NOTE(!): that you should only provide values that are part of the container definition document!
ecs_task_container_definition = "[{}]"
# The Docker networking mode to use for the containers in the task. The valid values are:
#   * none
#   * bridge
#   * awsvpc
#   * host
# NOTE(!): this is required to be "awsvpc" for "FARGATE" `launch_type`!
#ecs_task_network_mode = "awsvpc"
# The list of volume blocks definitions that containers in your task may use.
#ecs_task_volumes = []
# A set of placement constraints rules that are taken into consideration during task placement. Maximum
# number of `placement_constraints` is 10.
#ecs_task_placement_constraints = []
# The number of CPU units used by the task. If the `requires_compatibilities` is FARGATE this field is Required!
# If using `FARGATE` launch type `task_cpu` must match supported memory values:
#   * https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#task_size
#ecs_task_cpu = 256
# The amount of memory (in MiB) used by the task. If the `requires_compatibilities` is FARGATE this field is Required!
# If using Fargate launch type `task_memory` must match supported cpu value:
#   * https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#task_size
#ecs_task_memory = 512
# The launch type required by the task. The valid values are:
#   * EC2
#   * FARGATE
#ecs_task_requires_compatibilities = "FARGATE"
# The proxy configuration details for the App Mesh proxy. See working example:
#   * https://www.terraform.io/docs/providers/aws/r/ecs_task_definition.html#proxy-configuration-arguments
#ecs_task_proxy_configuration = {}
