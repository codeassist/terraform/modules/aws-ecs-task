# --------------------------
# Common/General parameters
# --------------------------
# Whether to create the resources ("false" prevents the module from creating any resources).
ecs_task_module_enabled = true
# Emulation of `depends_on` behavior for the module.
ecs_task_module_depends_on = []
# (Required) A unique name for your task definition.
ecs_task_family = "test4"
# Key-value mapping of resource tags.
ecs_task_tags = {}


# ---------------
# IAM parameters
# ---------------
# The JSON formatted string representing user-defined IAM policy to allow ECS Task call AWS Services on our behalf.
ecs_task_policy = ""
# In order to avoid error `count can't be computed` specify `true` here when a custom role must be associated.
ecs_task_custom_role_specified = false
# The ARN of custom IAM role to associate with the Task instead of creating one.
ecs_task_role_arn = ""


# --------------------
# ECS Task parameters
# --------------------
# (Required) A list of valid container definitions provided as a single valid JSON document.
# NOTE(!): that you should only provide values that are part of the container definition document!
ecs_task_container_definition = <<-JSON
[
  {
    "command":null,
    "cpu":256,
    "dependsOn":null,
    "dnsServers":null,
    "dockerLabels":null,
    "entryPoint":null,
    "environment":null,
    "essential":true,
    "firelensConfiguration":null,
    "healthCheck":null,
    "image":"docker:stable",
    "links":null,
    "logConfiguration":null,
    "memory":256,
    "memoryReservation":128,
    "mountPoints":null,
    "name":"test3",
    "portMappings": [
      {
        "containerPort":80,
        "hostPort":80,
        "protocol":"tcp"
      }
    ],
    "privileged":null,
    "readonlyRootFilesystem":false,
    "repositoryCredentials":null,
    "secrets":null,
    "startTimeout":30,
    "stopTimeout":30,
    "systemControls":null,
    "ulimits":null,
    "user":null,
    "volumesFrom":null,
    "workingDirectory":null
  }
]
JSON
# The Docker networking mode to use for the containers in the task. The valid values are:
#   * none
#   * bridge
#   * awsvpc
#   * host
# NOTE(!): this is required to be "awsvpc" for "FARGATE" `launch_type`!
ecs_task_network_mode = "awsvpc"
# The list of volume blocks definitions that containers in your task may use.
ecs_task_volumes = []
# A set of placement constraints rules that are taken into consideration during task placement. Maximum
# number of `placement_constraints` is 10.
#ecs_task_placement_constraints = []
# The number of CPU units used by the task. If the `requires_compatibilities` is FARGATE this field is Required!
# If using `FARGATE` launch type `task_cpu` must match supported memory values:
#   * https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#task_size
ecs_task_cpu = 256
# The amount of memory (in MiB) used by the task. If the `requires_compatibilities` is FARGATE this field is Required!
# If using Fargate launch type `task_memory` must match supported cpu value:
#   * https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#task_size
ecs_task_memory = 512
# The launch type required by the task. The valid values are:
#   * EC2
#   * FARGATE
ecs_task_requires_compatibilities = "FARGATE"
# The proxy configuration details for the App Mesh proxy. See working example:
#   * https://www.terraform.io/docs/providers/aws/r/ecs_task_definition.html#proxy-configuration-arguments
ecs_task_proxy_configuration = {
  # (Optional) The proxy type. The default value is APPMESH. The only supported value is APPMESH.
  type            = null
  # (Required) The name of the container that will serve as the App Mesh proxy.
  container_name  = "test4-proxy-container"
  # (Required) The set of network configuration parameters to provide the Container Network Interface (CNI) plugin,
  # specified a key-value mapping. See:
  #   * https://docs.aws.amazon.com/en_us/AmazonECS/latest/APIReference/API_ProxyConfiguration.html
  properties      = {
    # (Required) The user ID (UID) of the proxy container as defined by the user parameter in a container definition.
    # This is used to ensure the proxy ignores its own traffic. If `IgnoredGID` is specified, this field can be empty.
    IgnoredUID          = "0"
    # (Required) The group ID (GID) of the proxy container as defined by the user parameter in a container
    # definition. This is used to ensure the proxy ignores its own traffic. If `IgnoredUID` is specified, this field
    # can be empty.
    IgnoredGID          = null
    # (Required) The list of ports that the application uses. Network traffic to these ports is forwarded to the
    # `ProxyIngressPort` and `ProxyEgressPort`.
    AppPorts            = "8080"
    # (Required) Specifies the port that incoming traffic to the `AppPorts` is directed to.
    ProxyIngressPort    = 18080
    # (Required) Specifies the port that outgoing traffic from the `AppPorts` is directed to.
    ProxyEgressPort     = 18081
    # (Required) The egress traffic going to the specified ports is ignored and not redirected to the
    # `ProxyEgressPort`. It can be an empty list.
    EgressIgnoredPorts  = null
    # (Required) The egress traffic going to the specified IP addresses is ignored and not redirected to the
    # `ProxyEgressPort`. It can be an empty list.
    EgressIgnoredIPs    = "169.254.169.254"
  }
}
