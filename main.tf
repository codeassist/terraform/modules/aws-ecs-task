locals {
  enabled = var.ecs_task_module_enabled ? true : false

  tags = merge(
    var.ecs_task_tags,
    {
      terraform = true,
    },
  )
}


# ------------------------------------------------------------------------
# Setup required IAM resources (policies and role) to be used by ECS Task
# ------------------------------------------------------------------------
data "aws_iam_policy_document" "ecs_tasks_assume_role" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use with
  # resources which expect policy documents, such as the aws_iam_policy resource.
  count = local.enabled ? 1 : 0

  statement {
    sid     = "AllowECSTasksAndContainerAgentAndDockerDaemonAssumeTheRole"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "task_role" {
  # Provides an IAM role.
  #
  # NOTE: If policies are attached to the role via the `aws_iam_policy_attachment` resource and you are modifying the
  # role `name` or `path`, the `force_detach_policies` argument must be set to `true` and applied before attempting the
  # operation otherwise you will encounter a `DeleteConflict` error.
  # The `aws_iam_role_policy_attachment` resource (recommended) does not have this requirement.
  count = (local.enabled && var.ecs_task_custom_role_specified) ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = "ecs-task-role-"
  # (Optional) The description of the role.
  description = "The ECS Task [${var.ecs_task_family}] IAM role that allows your Amazon ECS container task to make calls to other AWS services."
  # Key-value mapping of tags for the IAM role.
  tags = merge(
    local.tags,
    map("Name", format("%s-ecs-task-role", var.ecs_task_family)),
  )
  # (Required) The policy that grants an entity permission to assume the role.
  assume_role_policy = join("", data.aws_iam_policy_document.ecs_tasks_assume_role.*.json)
}

# By default ECS Task doesn't require any permissions, see:
#   * https://docs.aws.amazon.com/en_us/AmazonECS/latest/developerguide/task_IAM_role.html
# This is minimal custom set of permissions.
data "aws_iam_policy_document" "task_minimal" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use with
  # resources which expect policy documents, such as the aws_iam_policy resource.
  count = (local.enabled && var.ecs_task_custom_role_specified) ? 1 : 0

  statement {
    sid       = "ECSTaskAllowedActions"
    effect    = "Allow"
    resources = ["*"]
    actions = [
      # to show CloudWatch logs in the AWS ECS Task console
      "logs:Describe*",
      "logs:Get*",
    ]
  }
}
data "aws_iam_policy_document" "empty" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use with
  # resources which expect policy documents, such as the aws_iam_policy resource.
  count = (local.enabled && var.ecs_task_custom_role_specified) ? 1 : 0
}
data "aws_iam_policy_document" "task_resulting_policy" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use
  # with resources which expect policy documents, such as the `aws_iam_policy` resource.
  count = (local.enabled && var.ecs_task_custom_role_specified) ? 1 : 0

  # (Optional) - An IAM policy document to import as a base for the current policy document.
  # Statements with non-blank SIDs in the `current` policy document will overwrite statements with the same SID in
  # the `source` JSON.
  # Statements without an SID cannot be overwritten.
  source_json = join("", data.aws_iam_policy_document.task_minimal.*.json)

  # (Optional) - An IAM policy document to import and override the current policy document.
  # Statements with non-blank SIDs in the `override` document will overwrite statements with the same SID in
  # the `current` document.
  # Statements without an SID cannot be overwritten.
  override_json = length(var.ecs_task_policy) > 0 ? var.ecs_task_policy : join("", data.aws_iam_policy_document.empty.*.json)
}

resource "aws_iam_role_policy" "task_custom" {
  # Provides an IAM role policy.
  count = (local.enabled && var.ecs_task_custom_role_specified) ? 1 : 0

  # (Optional) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = "ecs-task-policy-"
  # (Required) The policy document. This is a JSON formatted string.
  policy = join("", data.aws_iam_policy_document.task_resulting_policy.*.json)
  # (Required) The IAM role to attach to the policy.
  role = join("", aws_iam_role.task_role.*.id)
}


resource "aws_iam_role" "task_execution" {
  # Provides an IAM role.

  # NOTE: If policies are attached to the role via the `aws_iam_policy_attachment` resource and you are modifying the
  # role `name` or `path`, the `force_detach_policies` argument must be set to `true` and applied before attempting the
  # operation otherwise you will encounter a `DeleteConflict` error.
  # The `aws_iam_role_policy_attachment` resource (recommended) does not have this requirement.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = "ecs-task-execution-role-"
  # (Optional) The description of the role.
  description = "The ECS Task [${var.ecs_task_family}] IAM Execution role that the Amazon ECS container agent and the Docker daemon can assume."
  # Key-value mapping of tags for the IAM role.
  tags = merge(
    local.tags,
    map("Name", format("%s-ecs-task-execution-role", var.ecs_task_family)),
  )
  # (Required) The policy that grants an entity permission to assume the role.
  assume_role_policy = join("", data.aws_iam_policy_document.ecs_tasks_assume_role.*.json)
}
data "aws_iam_policy_document" "task_execution" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use with
  # resources which expect policy documents, such as the aws_iam_policy resource.
  count = local.enabled ? 1 : 0

  # See:
  #   * https://docs.aws.amazon.com/en_us/AmazonECS/latest/developerguide/task_execution_IAM_role.html

  statement {
    sid    = "AllowECSTasksExecuteSpecifiedActions"
    effect = "Allow"
    actions = [
      "ecr:GetAuthorizationToken",
      "ecr:BatchCheckLayerAvailability",
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]
    resources = ["*"]
  }

  statement {
    sid    = "RequiredPermissionsForPrivateRegistryAuthenticationAndAmazonECSSecrets"
    effect = "Allow"
    actions = [
      "kms:Decrypt",
      "secretsmanager:GetSecretValue",
      "ssm:GetParameters",
    ]
    resources = [
      "arn:aws:kms:*:*:key/*",
      "arn:aws:secretsmanager:*:*:secret:*",
      "arn:aws:ssm:*:*:parameter/*",
    ]
  }
}
resource "aws_iam_role_policy" "task_execution" {
  # Provides an IAM role policy.
  count = local.enabled ? 1 : 0

  # (Optional) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = "ecs-task-execution-role-policy-"
  # (Required) The policy document. This is a JSON formatted string.
  policy = join("", data.aws_iam_policy_document.task_execution.*.json)
  # (Required) The IAM role to attach to the policy.
  role = join("", aws_iam_role.task_execution.*.id)
}


# --------------------
# Create the ECS Task
# --------------------
resource "aws_ecs_task_definition" "this" {
  # Manages a revision of an ECS task definition to be used in aws_ecs_service.
  count = local.enabled ? 1 : 0

  depends_on = [
    var.ecs_task_module_depends_on
  ]

  # (Optional) Key-value mapping of resource tags.
  tags = local.tags

  # (Required) A unique name for your task definition.
  family = var.ecs_task_family
  # (Required) A list of valid container definitions (http://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_ContainerDefinition.html)
  # provided as a single valid JSON document. Please note that you should only provide values that are part of the
  # container definition document. For a detailed description of what parameters are available, see
  # the Task Definition Parameters section: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html
  # from the official Developer Guide: https://docs.aws.amazon.com/AmazonECS/latest/developerguide.
  container_definitions = var.ecs_task_container_definition
  # (Optional) The ARN of IAM role that allows your Amazon ECS container task to make calls to other AWS services.
  task_role_arn = var.ecs_task_role_arn != "" ? var.ecs_task_role_arn : join("", aws_iam_role.task_role.*.arn)
  # (Optional) The Amazon Resource Name (ARN) of the task execution role that the Amazon ECS container agent and the
  # Docker daemon can assume.
  execution_role_arn = join("", aws_iam_role.task_execution.*.arn)
  # (Optional) The Docker networking mode to use for the containers in the task. The valid values are:
  #   * none
  #   * bridge
  #   * awsvpc
  #   * host
  network_mode = var.ecs_task_network_mode

  # (Optional) A set of volume blocks that containers in your task may use.
  dynamic "volume" {
    for_each = var.ecs_task_volumes

    content {
      # (Required) The name of the volume. This name is referenced in the `sourceVolume` parameter of container
      # definition in the `mountPoints` section.
      name = volume.value.name
      # (Optional) The path on the host container instance that is presented to the container. If not set, ECS will
      # create a nonpersistent data volume that starts empty and is deleted after the task has finished.
      host_path = lookup(volume.value, "host_path", null)

      # Used to configure a docker volume. For more information, see Specifying a Docker volume in your Task Definition
      # Developer Guide:
      #   * https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-volumes.html#specify-volume-config
      dynamic "docker_volume_configuration" {
        for_each = lookup(volume.value, "docker_volume_configuration", [])

        content {
          # Optional) The scope for the Docker volume, which determines its lifecycle, either "task" or "shared".
          # Docker volumes that are scoped to a "task" are automatically provisioned when the task starts and
          # destroyed when the task stops.
          # Docker volumes that are scoped as "shared" persist after the task stops.
          scope = lookup(docker_volume_configuration.value, "scope", null)
          # (Optional) If this value is "true", the Docker volume is created if it does not already exist.
          # Note: This field is only used if the scope is "shared".
          autoprovision = lookup(docker_volume_configuration.value, "autoprovision", null)
          # (Optional) The Docker volume driver to use. The driver value must match the driver name provided by Docker
          # because it is used for task placement.
          driver = lookup(docker_volume_configuration.value, "driver", null)
          # (Optional) A map of Docker driver specific options.
          driver_opts = lookup(docker_volume_configuration.value, "driver_opts", null)
          # (Optional) A map of custom metadata to add to your Docker volume.
          labels = lookup(docker_volume_configuration.value, "labels", null)
        }
      }
    }
  }

  # (Optional) A set of placement constraints rules that are taken into consideration during task placement. Maximum
  # number of placement_constraints is 10.
  dynamic "placement_constraints" {
    for_each = var.ecs_task_placement_constraints
    content {
      # (Required) The type of constraint. Use "memberOf" to restrict selection to a group of valid candidates.
      # NOTE(!): that "distinctInstance" is not supported in task definitions.
      type = lookup(placement_constraints.value, "type", "")
      # (Optional) Cluster Query Language expression to apply to the constraint. For more information, see:
      #   * http://docs.aws.amazon.com/AmazonECS/latest/developerguide/cluster-query-language.html
      expression = lookup(placement_constraints.value, "expression", null)
    }
  }

  # (Optional) The number of cpu units used by the task. If the `requires_compatibilities` is "FARGATE" this field is
  # Required!
  cpu = var.ecs_task_cpu
  # (Optional) The amount (in MiB) of memory used by the task. If the `requires_compatibilities` is "FARGATE" this
  # field is Required!
  memory = var.ecs_task_memory
  # (Optional) A set of launch types required by the task. The valid values are:
  #   * EC2
  #   * FARGATE
  requires_compatibilities = [var.ecs_task_requires_compatibilities]

  # (Optional) The proxy configuration details for the App Mesh proxy.
  dynamic "proxy_configuration" {
    for_each = var.ecs_task_proxy_configuration != null ? [var.ecs_task_proxy_configuration] : []
    content {
      # (Optional) The proxy type. The default value is APPMESH. The only supported value is APPMESH.
      type = lookup(proxy_configuration.value, "type", "APPMESH")
      # (Required) The name of the container that will serve as the App Mesh proxy.
      container_name = lookup(proxy_configuration.value, "container_name", null)
      # (Required) The set of network configuration parameters to provide the Container Network Interface (CNI) plugin,
      # specified a key-value mapping. See:
      #   * https://docs.aws.amazon.com/en_us/AmazonECS/latest/APIReference/API_ProxyConfiguration.html
      properties = lookup(proxy_configuration.value, "properties", {})
    }
  }
}
