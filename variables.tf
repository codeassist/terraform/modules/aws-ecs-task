# --------------------------
# Common/General parameters
# --------------------------
variable "ecs_task_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # "false" prevents the module from creating any resources
  default = false
}

variable "ecs_task_module_depends_on" {
  description = "Emulation of `depends_on` behavior for the module."
  type        = any
  # Non zero length string can be used to have current module wait for the specified resource.
  default = null
}

variable "ecs_task_family" {
  description = "(Required) A unique name for your task definition."
  type        = string
}
variable "ecs_task_tags" {
  description = "Key-value mapping of resource tags."
  type        = map(string)
  default     = {}
}


# ---------------
# IAM parameters
# ---------------
variable "ecs_task_policy" {
  description = "The JSON formatted string representing user-defined IAM policy to allow ECS Task call AWS Services on our behalf."
  type        = string
  default     = ""
}
variable "ecs_task_custom_role_specified" {
  description = "In order to avoid error `count can't be computed` specify `true` here when a custom role must be associated."
  type        = bool
  default     = false
}
variable "ecs_task_role_arn" {
  description = "The ARN of custom IAM role to associate with the Task instead of creating one."
  type        = string
  default     = ""
}


# --------------------
# ECS Task parameters
# --------------------
variable "ecs_task_container_definition" {
  description = "(Required) A list of valid container definitions provided as a single valid JSON document."
  # NOTE(!): that you should only provide values that are part of the container definition document!
  type = string
}
variable "ecs_task_network_mode" {
  description = "The Docker networking mode to use for the containers in the task."
  # The valid values are:
  #   * none
  #   * bridge
  #   * awsvpc
  #   * host
  # NOTE(!): this is required to be "awsvpc" for "FARGATE" `launch_type`!
  type    = string
  default = "awsvpc"
}
variable "ecs_task_volumes" {
  description = "The list of volume blocks definitions that containers in your task may use."
  type = list(object({
    # (Optional) The path on the host container instance that is presented to the container. If not set, ECS will
    # create a nonpersistent data volume that starts empty and is deleted after the task has finished.
    host_path = string
    # (Required) The name of the volume. This name is referenced in the sourceVolume parameter of container
    # definition in the mountPoints section.
    name = string
    # (Optional) Used to configure a docker volume:
    docker_volume_configuration = list(object({
      # (Optional) The scope for the Docker volume, which determines its lifecycle, either:
      #   * task    - Docker volumes that are scoped to a "task" are automatically provisioned when the task starts
      #               and destroyed when the task stops.
      #   * shared  - Docker volumes that are scoped as "shared" persist after the task stops.
      scope = string
      # (Optional) If this value is "true", the Docker volume is created if it does not already exist.
      # NOTE(!): This field is only used if the `scope` is "shared".
      autoprovision = bool
      # (Optional) The Docker volume driver to use. The driver value must match the driver name provided by Docker
      # because it is used for task placement.
      driver = string
      # (Optional) A map of Docker driver specific options.
      driver_opts = map(string)
      # (Optional) A map of custom metadata to add to your Docker volume.
      labels = map(string)
    }))
  }))
  default = []
}
variable "ecs_task_placement_constraints" {
  description = "A set of placement constraints rules that are taken into consideration during task placement. Maximum number of `placement_constraints` is 10."
  # See:
  #   * https://www.terraform.io/docs/providers/aws/r/ecs_task_definition.html#placement-constraints-arguments
  #   * https://aws.amazon.com/ru/blogs/compute/amazon-ecs-task-placement/
  type = list(object({
    # (Required) The type of constraint. Use "memberOf" to restrict selection to a group of valid candidates.
    # NOTE(!): that "distinctInstance" is not supported in task definitions.
    type = string
    # (Optional) Cluster Query Language expression to apply to the constraint. For more information, see:
    #   * http://docs.aws.amazon.com/AmazonECS/latest/developerguide/cluster-query-language.html
    expression = string
  }))
  default = []
}
variable "ecs_task_cpu" {
  description = "The number of CPU units used by the task. If the `requires_compatibilities` is FARGATE this field is Required!"
  # If using `FARGATE` launch type `task_cpu` must match supported memory values:
  #   * https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#task_size
  type    = number
  default = 256
}
variable "ecs_task_memory" {
  description = "The amount of memory (in MiB) used by the task. If the `requires_compatibilities` is FARGATE this field is Required!"
  # If using Fargate launch type `task_memory` must match supported cpu value:
  #   * https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html#task_size
  type    = number
  default = 512
}
variable "ecs_task_requires_compatibilities" {
  description = "The launch type required by the task. The valid values are EC2 or FARGATE."
  type        = string
  default     = "FARGATE"
}
variable "ecs_task_proxy_configuration" {
  description = "The proxy configuration details for the App Mesh proxy."
  # See: https://www.terraform.io/docs/providers/aws/r/ecs_task_definition.html#proxy-configuration-arguments for
  # working example
  type = object({
    # (Optional) The proxy type. The default value is APPMESH. The only supported value is APPMESH.
    type = string
    # (Required) The name of the container that will serve as the App Mesh proxy.
    container_name = string
    # (Required) The set of network configuration parameters to provide the Container Network Interface (CNI) plugin,
    # specified a key-value mapping. See:
    #   * https://docs.aws.amazon.com/en_us/AmazonECS/latest/APIReference/API_ProxyConfiguration.html
    properties = object({
      # (Required) The user ID (UID) of the proxy container as defined by the user parameter in a container definition.
      # This is used to ensure the proxy ignores its own traffic. If `IgnoredGID` is specified, this field can be empty.
      IgnoredUID = string
      # (Required) The group ID (GID) of the proxy container as defined by the user parameter in a container
      # definition. This is used to ensure the proxy ignores its own traffic. If `IgnoredUID` is specified, this field
      # can be empty.
      IgnoredGID = string
      # (Required) The list of ports that the application uses. Network traffic to these ports is forwarded to the
      # `ProxyIngressPort` and `ProxyEgressPort`.
      AppPorts = string
      # (Required) Specifies the port that incoming traffic to the `AppPorts` is directed to.
      ProxyIngressPort = number
      # (Required) Specifies the port that outgoing traffic from the `AppPorts` is directed to.
      ProxyEgressPort = number
      # (Required) The egress traffic going to the specified ports is ignored and not redirected to the
      # `ProxyEgressPort`. It can be an empty list.
      EgressIgnoredPorts = string
      # (Required) The egress traffic going to the specified IP addresses is ignored and not redirected to the
      # `ProxyEgressPort`. It can be an empty list.
      EgressIgnoredIPs = string
    })
  })
  default = null
}
